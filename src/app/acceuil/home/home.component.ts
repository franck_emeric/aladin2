import { Component, OnInit ,OnChanges, OnDestroy} from '@angular/core';
import { AladinService,LocalService } from 'src/app/core';
declare var require: any;

var $ = require("jquery");
declare  var require:any;
var myalert=require('sweetalert2');
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit ,OnChanges,OnDestroy{
  name:any;
  id:any;
  hide=false
   hidepack=false
   hidebody=true
   details:any={}
  show: boolean = true;
  private subscription:any
  data={}
  cart=false
  editor=false;
  sho=true
  fromhome=true
  constructor(private aladin:AladinService,private l:LocalService) { }

  ngOnInit(): void {
   
    console.log(this.data)
    this.aladin.ShowEditor.subscribe(mess=>{
      if(!mess.show){
        this.sho=mess.show
        this.show= mess.show;
        this.data=mess;
        this.editor= !mess.show;
        this.cart= mess.show;
        $(document).ready(function () {
          $('html, body').animate({
              scrollTop: $('#editor').offset().top
          }, 'slow');
          
      });
      }
     
      
  })
   this.l.activatecart.subscribe(res=>{
     if(res){
       this.sho=res
      this.cart=res
      this.show=!res;
      this.editor=!res;
      $(document).ready(function () {
        $('html, body').animate({
            scrollTop: $('#cart').offset().top
        }, 'slow');
    });

     }else{
      this.cart=res
      this.sho=!res
      this.show=!res;
      this.editor=res;
      
     }

   })


  }

ngOnChanges(){
 

}


ngOnDestroy(){
  this.subscription.unsubscribe()
}
  toggleSpinner(){
    this.show = !this.show;   
  }

}
