import { Component, OnInit } from '@angular/core';
import { HttpService, ListService } from 'src/app/core';
import { AladinService } from 'src/app/core';
import { fabric } from 'fabric';
@Component({
  selector: 'app-listprint',
  templateUrl: './listprint.component.html',
  styleUrls: ['./listprint.component.scss']
})
export class ListprintComponent implements OnInit {
packs:any;
url="/editor/packs/";
shw=true
propacks:any={}
uploadedurl:any;
details:any={};
crea=false;
showchoices=false;
estsac=false;
estsachet=false;
models:any
produit:any=[]
hide=false
text="Je veux imprimer ma créa !!"
hideprice:Boolean=true
 hidepromo:Boolean=false

 jday= new Date().getDay()

  constructor(private l:ListService, private uplod: AladinService, private http:HttpService) { }

  ngOnInit(): void {
    if(this.jday==5){
      this.hideprice=false
      this.hidepromo=true
    }

    this.http.get().subscribe(res=>{
     this.models=res
      for(let item of this.models){      
        item.description = JSON.parse(item.description);
        if(this.IsJsonString(item.objf) && JSON.parse(item.objf)!=null){
          this.getCanvasUrl2(JSON.parse(item.objf),item).then(async (res)=>{
             
           }).catch((err)=>{ 
             console.log(err)
           })
         }
        if(this.IsJsonString(item.obj)){
        this.getCanvasUrl(JSON.parse(item.obj),item).then(async (res)=>{
       
        }).catch((err)=>{ 
          console.log(err)
        })
       }

     
    
      }
    },
    err=>{
      console.log(err)
    })
  }

  async getCanvasUrl(obj:any,item:any){
     var  product:any
    let canvas= new fabric.Canvas(null,{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false,
    });
   return await  canvas.loadFromJSON(obj,(ob:any)=>{
    canvas.setHeight(item.height)
    canvas.setWidth(item.width)
    if(JSON.parse(item.objf)!=null){
      product={
        url:canvas.toDataURL(),
        url2:this.propacks.url2,
        price:item.description.price,
        promo:item.description.promo,
        size:item.description.size,
        type:item.description.type,
        name:item.description.name,
        qty:item.description.qty,
        owner:item.owner,
        comment:item.description.made_with,
        item:item,
        width: item.width,
        height: item.height
  
      }     
      if(item.category=="2"){
       this.produit.push(product);
    
      }
    }else if(JSON.parse(item.objf)==null){
      product={
        url:canvas.toDataURL(),
        url2:null,
        price:item.description.price,
        promo:item.description.promo,
        size:item.description.size,
        type:item.description.type,
        name:item.description.name,
        qty:item.description.qty,
        owner:item.owner,
        comment:item.description.made_with,
        item:item,
        width: item.width,
        height: item.height
  
      }     
      if(item.category=="2"){
       this.produit.push(product);
    
      }
    }
     
   })
  }

  async getCanvasUrl2(objf:any,item:any){   
    let canvas= new fabric.Canvas(null,{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false,
      });
     return await  canvas.loadFromJSON(objf,(ob:any)=>{
    canvas.setHeight(item.height)
    canvas.setWidth(item.width)
     this.propacks={
      url2:canvas.toDataURL(),
    }
     
   })
  }

  IsJsonString(str:any) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }


   OnclickSac(){
    if(this.estsac==false){
      this.text="J'importe mon visuel de sac";
      this.showchoices=true;
      this.estsachet=false;
      this.estsac=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  

   }

displaychoices(){
  this.showchoices=true;
}

   OnclickSachet(){
    if(this.estsachet==false){
      this.text="J'importe mon visuel de sachet";
      this.showchoices=true;
      this.estsachet=true;
      this.estsac=false
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }

   ChangeComponent(value:boolean){
     this.shw=value;
     this.showchoices=false;
     this.text="Je veux imprimer ma créa !!";
     this.estsac=false;
     this.estsachet=false;
   }

   Upload(event:any){
    let file =event.target.files[0]
    if(!this.uplod.UpleadImage(file)){
    const reader = new FileReader();
  reader.onload = () => {
  
   this.uploadedurl = reader.result;
   
   };
   reader.readAsDataURL(file);

   this.show()
    console.log(event)
  }else{
    
  }
  }

   Changecomponent(value:boolean){
    this.shw=value;
 }

show(){
  this.crea=true;
  this.shw=false;
}
  
letchange(value:boolean){
  this.shw=value
  this.crea=!value
}

showDetails(data:any){
    this.hide=true
    this.shw=!this.show
    Object.assign(this.details,{url:data.url,url2:data.url2,owner:data.owner,comment:data.comment,price:data.price,promo:data.promo,name:data.name,size:data.size,show:true, item:data.item, width:data.width, height:data.height})
}
Showaladin(data:any){
  Object.assign(this.details,data)  
}

}
