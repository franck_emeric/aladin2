import { Component, OnInit } from '@angular/core';
import { FormvalidationService } from 'src/app/core';
import { ForgotpwdService } from 'src/app/core';

@Component({
  selector: 'app-pwdforgot',
  templateUrl: './pwdforgot.component.html',
  styleUrls: ['./pwdforgot.component.scss']
})
export class PwdforgotComponent implements OnInit {
email="";
user:any
newpwd="";
newpwdc="";
ismail=false;
ispwdm=false;
msg={
  email:"",
  pwd:""
}
  constructor(private validate:FormvalidationService,private forgotpwd:ForgotpwdService) { }

  ngOnInit(): void {
  }

  Updatepwd(){
    if(this.email!=null&&this.newpwd!=null){
      if(this.validate.validateEmail(this.email)){
       
        this.forgotpwd.passwordforgot({email:this.email,password:this.newpwd})
        
        
      }
      else{
        this.ismail=!this.ismail;
        this.msg.email="invalide email";
      }

    }


  }

}
