import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescpackagesComponent } from './descpackages.component';

describe('DescpackagesComponent', () => {
  let component: DescpackagesComponent;
  let fixture: ComponentFixture<DescpackagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescpackagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescpackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
